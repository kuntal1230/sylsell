<?php
/**
 * Created by PhpStorm.
 * User: EXPERT SOFT IT
 * Date: 4/25/2017
 * Time: 12:15 PM
 */

namespace App\Product;


use App\Dbconect\Dbconect;
use PDO;

class Product extends Dbconect
{

    public function addProduct($data = '')
    {

        if (!empty($_FILES['img1']['name'])) {
            $name1 = $_FILES['img1']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img1"]["tmp_name"], "$folder" . "$name1");
        }else{$name1 = null;}
        if (!empty($_FILES['img2']['name'])) {
            $name2 = $_FILES['img2']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img2"]["tmp_name"], "$folder" . "$name2");
        }else{$name2 = null;}
        if (!empty($_FILES['img3']['name'])) {
            $name3 = $_FILES['img3']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img3"]["tmp_name"], "$folder" . "$name3");
        }else{$name3 = null;}
        if (!empty($_FILES['img4']['name'])) {
            $name4 = $_FILES['img4']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img4"]["tmp_name"], "$folder" . "$name4");
        }else{$name4 = null;}
        if (!empty($_FILES['img5']['name'])) {
            $name5 = $_FILES['img5']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img5"]["tmp_name"], "$folder" . "$name5");
        }else{$name5 = null;}
        if (!empty($_FILES['img6']['name'])) {
            $name6 = $_FILES['img6']['name'];
            $folder = $_SERVER['DOCUMENT_ROOT'] . '/assets/product_img/';
            move_uploaded_file($_FILES["img6"]["tmp_name"], "$folder" . "$name6");
        }else{$name6 = null;}


        if (isset($data['product_name'])) {
            $name = $data['product_name'];
        }
        if (isset($data['product_price'])) {
            $price = $data['product_price'];
        }
        if (isset($data['made-in'])) {
            $madein = $data['made-in'];
        }
        if (isset($data['product-warrenty'])) {
            $warrenty = $data['product-warrenty'];
        }
        if (isset($data['product-guarrenty'])) {
            $guarrenty = $data['product-guarrenty'];
        }
        if (isset($data['product-replacement'])) {
            $replacement = $data['product-replacement'];
        }
        if (isset($data['money-back'])) {
            $moneyback = $data['money-back'];
        }
        if (isset($data['product-otherinfo'])) {
            $info = $data['product-otherinfo'];
        }
        if (isset($data['within24hours'])) {
            $withen24 = $data['within24hours'];
        }else{$withen24 = 'No';}
        if (isset($data['within72hours'])) {
            $withen72 = $data['within72hours'];
        }else{$withen72= 'No';}
        if (isset($data['within96hours'])) {
            $withen96 = $data['within96hours'];
        }else{$withen96= 'No';}
        if (isset($data['withinDhaka'])) {
            $withindhaka = $data['withinDhaka'];
        }else{$withindhaka= 'No';}
        if (isset($data['outofDhaka'])) {
            $outofdhaka = $data['outofDhaka'];
        }else{$outofdhaka= 'No';}
        if (isset($data['fromoffice'])) {
            $fromoffice = $data['fromoffice'];
        }else{$fromoffice= 'No';}
        if (isset($data['homeDelivery'])) {
            $homedelivery = $data['homeDelivery'];
        }else{$homedelivery= 'No';}

        if (isset($data['bank'])) {
            $bank = $data['bank'];
        }else{$bank= 'No';}
        if (isset($data['bcash'])) {
            $bcash = $data['bcash'];
        }else{$bcash= 'No';}
        if (isset($data['ucash'])) {
            $ucash = $data['ucash'];
        }else{$ucash= 'No';}

        if (isset($data['cashondelivery'])) {
            $cashondelivery = $data['cashondelivery'];
        }else{$cashondelivery= 'No';}
        $pdo = $this->pdo;
        $query = "SELECT * FROM products";
        $list = $pdo->query($query);
        $totalRow = $list->rowCount();
         $row = $totalRow+1;
        $code = "SS-".$row;
        if (!empty($name) && !empty($price)) {
            $pdo = $this->db;

            $query = 'INSERT INTO `products` (`user_id`,`product_code`,`product_name`,`product_price`, `product_madein`, `product_warrenty`, `product_garrenty`, `product_replacement`, `product_moneyback`, `product_otherinfo`, `created_at`) 
VALUES(:uid, :code, :name, :price, :madein, :warrenty, :garrenty, :replacement, :moneyback, :otherinfo, :time)';
            $time = date('Y-m-d H:i:s');
            $stmt = $pdo->prepare($query);
            $data = array(
                ':uid' => $_SESSION['author-user']['id'],
                ':code' => $code,
                ':name' => $name,
                ':price' => $price,
                ':madein' => $madein,
                ':warrenty' => $warrenty,
                ':garrenty' => $guarrenty,
                ':replacement' => $replacement,
                ':moneyback' => $moneyback,
                ':otherinfo' => $info,
                ':time' => $time
            );
            $status = $stmt->execute($data);
            $lastid = $pdo->lastInsertId();
            if ($status) {
                $pdo = $this->db;
                $query = 'INSERT INTO `delivery_option` (`product_id`,`within24hours`, `within72hours`, `within96hours`, `withindhaka`, `outofdhaka`, `fromoffice`, `homedelivery`, `created_at`) 
                      VALUES(:pid, :within24, :within72, :within96, :withindhaka, :outofdhaka, :fromoffice, :homedelivery, :time)';
                $time = date('Y-m-d H:i:s');
                $stmt = $pdo->prepare($query);
                $data = array(
                    ':pid' => $lastid,
                    ':within24' => $withen24,
                    ':within72' => $withen72,
                    ':within96' => $withen96,
                    ':withindhaka' => $withindhaka,
                    ':outofdhaka' => $outofdhaka,
                    ':fromoffice' => $fromoffice,
                    ':homedelivery' => $homedelivery,
                    ':time' => $time

                );
                $stmt->execute($data);
                $pdo = $this->db;
                $query = 'INSERT INTO `payment_option` (`product_id`,`bank`, `bkash`, `ukash`, `cashondelivery`, `created_at`) 
                      VALUES(:pid, :bank, :bkash, :ukash, :cash, :time)';
                $time = date('Y-m-d H:i:s');
                $stmt = $pdo->prepare($query);
                $data = array(
                    ':pid' => $lastid,
                    ':bank' => $bank,
                    ':bkash' => $bcash,
                    ':ukash' => $ucash,
                    ':cash' => $cashondelivery,
                    ':time' => $time

                );
                $stmt->execute($data);
                $pdo = $this->db;
                $query = 'INSERT INTO `product_image` (`product_id`,`image_1`, `image_2`, `image_3`, `image_4`,`image_5`,`image_6`, `created_at`) 
                      VALUES(:pid, :img1, :img2, :img3, :img4,:img5, :img6, :time)';
                $time = date('Y-m-d H:i:s');
                $stmt = $pdo->prepare($query);
                $data = array(
                    ':pid' => $lastid,
                    ':img1' => $name1,
                    ':img2' => $name2,
                    ':img3' => $name3,
                    ':img4' => $name4,
                    ':img5' => $name5,
                    ':img6' => $name6,
                    ':time' => $time

                );
                $stmt->execute($data);

            }


        }

    }

    public function viewProductByUser(){
        $pdo = $this->db;
        $query = 'SELECT products.*, product_image.*, payment_option.*, delivery_option.* FROM products LEFT JOIN product_image ON products.id = product_image.product_id JOIN payment_option ON products.id = payment_option.product_id JOIN delivery_option ON products.id = delivery_option.product_id WHERE user_id=:id';
        $stmt = $pdo->prepare($query);
    $data = array(
        ':id' => $_SESSION['author-user']['id']
    );
    $stmt->execute($data);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function viewProduct($per_page,$condition){
        $pdo = $this->db;
        //for pagination start
        if (isset($_GET['page'])) {

            $page = $_GET['page'];

        } else {

            $page = 1;

        }
        $start = ($page - 1) * $per_page;
        //for pagination end
        $query = "SELECT products.*, product_image.*, payment_option.*, delivery_option.* FROM products  $condition limit $start, $per_page";
        $result = $pdo->query($query);

        return $result;
    }

    public function viewProductById($data = ''){
        $id = $data['id'];
        $pdo = $this->db;
        $query = 'SELECT products.*, product_image.*, payment_option.*, delivery_option.* FROM products LEFT JOIN product_image ON products.id = product_image.product_id JOIN payment_option ON products.id = payment_option.product_id JOIN delivery_option ON products.id = delivery_option.product_id WHERE products.id = :id';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':id' => $id
        );
        $stmt->execute($data);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function HomeSlider(){
        $pdo = $this->db;
        $query = 'SELECT * FROM product_image  ORDER BY id DESC LIMIT 20';
        $result = $pdo->query($query);

        return $result;
    }

    public function product_info_by_code($product_code){
        $pdo = $this->db;
        $query=$pdo->prepare("select * from products where product_code=:product_code");
        $data=array(
            ":product_code" => $product_code
        );
        $query->execute($data);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    public function order(){
        extract($_POST);
        $pdo = $this->db;
        $query=$pdo->prepare("insert into orders(uploader,order_id,name,contact_number,product_codes,delevery_address,total_quantity,total_price,payment_method,created_at) 
                                        values(:uploader,:order_id,:name,:contact_number,:product_codes,:delevery_address,:total_quantity,:total_price,:payment_method,:created_at)");
        $date=date("Y-m-d H:i:s");
        $data=array(
            ":uploader" => $uploader,
            ":order_id" => $order_id,
            ":name" => $name,
            ":contact_number" => $contact_number,
            ":product_codes" => $product_codes,
            ":delevery_address" => $delevery_address,
            ":total_quantity" => $total_quantity,
            ":total_price" => $total_price,
            ":payment_method" => $payment_method,
            ":created_at" => $date
        );
        $send=$query->execute($data);
        if($query){
            $i=0;
            foreach ($_POST['product_code'] as $product_code){
                $quantity=$_POST['quantity'][$i];
                $query=$pdo->prepare("insert into order_items(order_id,product_code,quantity,created_at) values(:order_id,:product_code,:quantity,:created_at)");
                $data=array(
                    ":order_id" => $order_id,
                    ":product_code" => $product_code,
                    ":quantity" => $quantity,
                    ":created_at" => $date
                );
                $query->execute($data);
            }
            if($query){
                $url=SITE_URL."/page/success";
                header("location:$url");
            }

        }

    }//order
    public function paginate($table, $per_page = '', $stage_number = 3, $condition = '')
    {

        $pdo = $this->db;
            $query = "SELECT * FROM $table $condition";
            $list = $pdo->query($query);
        $a = $list->rowCount();
        if ($a > 0) {
            $total_page = ceil($a / $per_page);
            if (isset($_GET['page'])) {
                $page = $_GET['page'];
            } else {
                $page = 1;
            }
            if($page+$stage_number <=$total_page){
                $stage=$page+$stage_number;
            }
            else{
                $stage=$total_page-$page+$page;
            }
            //for extra when user select name sorting
            function url_maker($page_number){
                $url=parse_url($_SERVER['REQUEST_URI']);
                $path=$url['path'].'/';
                if(isset($url['query'])){
                    $url_arg=$url['query'];
                }
                else{
                    $url_arg='';
                }

                $delete_page_arg=preg_replace('/page\=[0-9]+/','',$url_arg);
                if($url_arg>8){
                    $combiner="&";
                }
                $combiner="";
                $final_url_arg="?page=".$page_number.$combiner.$delete_page_arg;
                $final_full_url=$path.$final_url_arg;
                return $final_full_url=preg_replace('/\/+/','/',$final_full_url);
            }




            echo "<ul class='pagination'>";
            if($page>1){
                $pre_page=$page-1;
                $f_url=url_maker($pre_page);
                echo "<li><a href='$f_url' class='pre-page'>Previous Page</a></li>";
            }

            for ($i = $page; $i <= $stage; $i++) {
                echo "<li>";
                $f_url=url_maker($i);
                if ($i == $page) {
                    echo "<a href='$f_url' style='color:red;text-decoration:none' class='active'>" . $i . "</a>";
                } else {
                    echo "<a href='$f_url'>" . $i . "</a>&nbsp;&nbsp;";
                }
                echo "</li>";
            }

            if($page<$total_page){
                $next_page=$page+1;
                $f_url=url_maker($next_page);
                echo "<li><a href='$f_url' class='next-page'>Next Page</a></li>";
            }
            echo "</ul>";
        }
    }// end of paginate

    public function orderList($per_page,$condition){
        $pdo = $this->db;
        //for pagination start
        if (isset($_GET['page'])) {

            $page = $_GET['page'];

        } else {

            $page = 1;

        }
        $start = ($page - 1) * $per_page;
        //for pagination end
        $query = "SELECT orders.*, order_items.*,products.* FROM orders  $condition limit $start, $per_page";
        $result = $pdo->query($query);

        return $result;

    }

    public function viewOrderDetailesById($data = ''){

            $orderId = $data;

        $pdo = $this->db;
        $query = 'SELECT * FROM orders WHERE order_id =:id';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':id' => $orderId
        );
        $stmt->execute($data);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function viewOrderProductDetailesById($data = ''){
        $orderId = $data;
        $pdo = $this->db;
        $query = 'SELECT order_items.*,products.*,authorised_user.* FROM order_items LEFT JOIN products ON order_items.product_code=products.product_code LEFT JOIN authorised_user ON products.user_id=authorised_user.id WHERE order_items.order_id=:id';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':id' => $orderId
        );
        $stmt->execute($data);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function confirm($data = '')
    {

            $pdo = $this->db;


            $query = 'update  orders set activity=:activity , updated_at=:confirm WHERE order_id =:id';
        $time = date('Y-m-d H:i:s');
            $stmt = $pdo->prepare($query);
            $data = array(
                ':id' => $data['id'],
                ':activity' => 1,
                ':confirm' => $time

            );
            $result = $stmt->execute($data);

            if ($result) {
                header('location:' . SITE_URL . '/admin/order');
            }

    }
}
