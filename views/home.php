<?php
use App\Auth\Auth;
use App\Product\Product;
$auth = new Auth();
$product = new Product();
if (isset($_POST['register'])){
    $result = $auth->register($_POST);
}
if (isset($_POST['login'])){
    $result = $auth->logim($_POST);
}
if (isset($_POST['author'])){
    $result = $auth->authorisetion($_POST);
}
$image = $product->HomeSlider();
?>
   <div class="section go container-fluid text-center">
       <div class="logo photo">
           <img src="../assets/img/logo.png">
           
       </div>
       <a href="<?php echo SITE_URL; ?>/page/product"><button class="go-button">Go</button></a>
       <div class="clearfix"></div>
       <div class="warning">
           <?php if(isset($result)):?>
               <p>
                   <?php echo $result;?>
               </p>
           <?php endif ?>

       </div>
   </div><!-- end go section with logo -->
   <div class="login-registration container-fluid">
        <div class="row">
            <div class="col-md-3 registration">
                <div class="row">

                    <form action="" method="post">
                        <div class="col-md-10">
                            <h3>
                                Free Membership
                                <span>
                                    READ
                                </span>
                            </h3>
                            <input type="text" class="input" placeholder="User Name" name="name">
                            <input type="text" class="input" placeholder="Mobile Number" name="number">
                            <input type="email" class="input" placeholder="Eamil" name="email">
                            <input type="password" class="input" placeholder="Password" name="password">
                            <input type="password" class="input" placeholder="Re-Password" name="re-password">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name="register" class="ok-button">Ok</button>
                        </div>
                    </form>
                </div>
                
            </div> 
            <div class="col-md-3 login">
                <h3>Log in</h3>
                <form action="" method="post">
                    <input type="text" class="input" placeholder="Mobile Number" name="number">
                    <input type="password" class="input" placeholder="Password" name="password">
                    <button type="submit" name="login" class="apply-button">Apply</button>
                </form>
            </div> 
            <div class="col-md-3 authorize pull-right">
                <p>Authorised Persons Only</p>
                <form action="" method="post">
                    <input type="text" class="input author-number-input" placeholder="12 Degit Code" name="code">
                    <button type="submit" name="author" class="author-go">Go</button>
                </form>
            </div> 
            <div class="col-md-12">
                <div class="owl-carousel home-slider">
                    <?php foreach ($image as $img) { ?>
                  <div><a href="<?php echo SITE_URL.'/page/view/?id='.$img['product_id'] ?>"> <img src="<?php echo SITE_URL.'/assets/product_img/'.$img['image_1'];?>"></a> </div>
                    <?php } ?>

                </div>
            </div>
        </div>
   </div><!-- end login registration -->
