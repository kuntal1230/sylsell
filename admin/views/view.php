<?php
use App\Product\Product;

$orderDetails = new Product();
if (isset($_GET['id'])){
    $id = $_GET['id'];
}
    $result = $orderDetails->viewOrderDetailesById($id);

   $product = $orderDetails->viewOrderProductDetailesById($id);

?>
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Order Details
                    </div>

                    <div class="panel-body">
                        <div class="row user-info">
                            <p class="col-md-6">
                                <span><?php echo $result['name'] ?></span>
                                <span>Contact address</span>
                                <?php echo $result['delevery_address'] ?>
                                <span>mobile number</span><?php echo $result['contact_number'] ?>
                            </p>
                            <p class="col-md-6">
                                <span>Total Quantity</span>
                                <?php echo $result['total_quantity'] ?>
                                <span>Total Price</span><?php echo $result['total_price'] ?> Tk.
                                <span>Payment Method</span> <?php echo $result['payment_method'] ?>
                                <span>Order Number</span> <?php echo $result['order_id'] ?>
                            </p>
                        </div>
                        <?php foreach ($product as $value) { ?>
                            <h4>Serial 1</h4>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th>Product Code</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Store Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td><?php echo $value['product_name']?></td>
                                        <td><?php echo $value['product_code']?></td>
                                        <td><?php echo $value['quantity']?></td>
                                        <td><?php echo $value['product_price']?></td>
                                        <td><?php echo $value['name_of_business']?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
        <!-- /. ROW  -->