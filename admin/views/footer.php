<!-- JQUERY SCRIPTS -->
<script src="<?php echo SITE_URL ?>/admin/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="<?php echo SITE_URL ?>/admin/assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="<?php echo SITE_URL ?>/admin/assets/js/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="<?php echo SITE_URL ?>/admin/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="<?php echo SITE_URL ?>/admin/assets/js/morris/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="<?php echo SITE_URL ?>/admin/assets/js/custom.js"></script>


</body>
</html>
