<?php
use App\Auth\Auth;
$auth =new Auth();
$authoriseduser = $auth->getAuthorisedUser();
?>
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Authorised User List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Shope Name</th>
                                    <th>Area name</th>
                                    <th>Number</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($authoriseduser as $value){ ?>
                                <tr>
                                    <td><?php echo $value['associate_name'] ?></td>
                                    <td><?php echo $value['name_of_business'] ?></td>
                                    <td><?php echo $value['name_of_area'] ?></td>
                                    <td><?php echo $value['authorise_user_number'] ?></td>
                                    <td><?php if ($value['activity']==0){
                                        echo "Not Active";
                                        }else{
                                        echo "Active";
                                        }  ?></td>
                                    <td>
                                        <?php if ($value['activity']==0){ ?>
                                            <a href="<?php echo SITE_URL.'/admin/useres/?id='?>">Active</a>
                                       <?php }else{ ?>
                                            <a href="<?php echo SITE_URL.'/admin/useres/?id='?>">De Active</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. ROW  -->