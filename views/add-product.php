<?php
use App\Product\Product;
$product = new Product();
if (!isset($_SESSION['author-user'])){
    header('location:' . SITE_URL . '/page/home');

}
if (isset($_POST['add-product'])){
 $product->addProduct($_POST);
}
$result = $product->viewProductByUser();
?>
<div class="structure  container-fluid add-page">
        <div class="row">
            <div class="mini-logo  col-md-2">
               <img src="../assets/img/logo.png">
           </div><!-- end logo -->
           <div class="content col-md-10 user-info">
                <p class="text-right">
                    <span><?php
                    echo $_SESSION['author-user']['name_of_business'];
                        ?></span>
                    <span><?php
                        echo $_SESSION['author-user']['authorise_user_number'];
                        ?></span>
                    <span><?php
                        echo $_SESSION['author-user']['name_of_area'];
                        ?></span>
                </p>
           </div>
        </div>
   </div>
   <form action="" method="post" enctype="multipart/form-data">
   <div class="container-fluid add-items-place">
       <div class="row">
           <div class="col-md-2">
               <p>
                    <label class="upload_item item1">
                       Lower Size 
                       <input type="file" class="hidden" name="img1">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           <div class="col-md-2">
               <p>
                    <label class="upload_item item2">
                       Lower Size
                       <input type="file" class="hidden" name="img2">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           <div class="col-md-2">
               <p>
                    <label class="upload_item item3">
                       Lower Size
                       <input type="file" class="hidden" name="img3">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           <div class="col-md-2">
               <p>
                    <label class="upload_item item4">
                       Lower Size
                       <input type="file" class="hidden" name="img4">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           <div class="col-md-2">
               <p>
                    <label class="upload_item item5">
                       Lower Size
                       <input type="file" class="hidden" name="img5">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           <div class="col-md-2">
               <p>
                    <label class="upload_item item6">
                       Lower Size
                       <input type="file" class="hidden" name="img6">
                        <span><img src=""></span>
                   </label>
               </p>
           </div>
           
           
       </div>
   </div>
   <div class="container-fluid product-details">
       <div class="row">
           <div class="col-md-6 basic-info">
               <div class="row">
                   <div class="">
                       <span class="description-heading">Product Description</span>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Name Of Product
                   </div>
                   <div class="col-md-9">
                       <input type="text" name="product_name">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Product Price
                   </div>
                   <div class="col-md-9">
                       <input type="text" name="product_price">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Made In
                   </div>
                   <div class="col-md-9">
                       <input type="text" name="made-in" >
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Warrenty
                   </div>
                   <div class="col-md-9">
                       <select name="product-warrenty" id="">
                           <option value="Yes">Yes</option>
                           <option value="No">No</option>
                       </select>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Guarrenty
                   </div>
                   <div class="col-md-9">
                       <select name="product-guarrenty" id="">
                           <option value="Yes">Yes</option>
                           <option value="No">No</option>
                       </select>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Replacement
                   </div>
                   <div class="col-md-9">
                       <select name="product-replacement" id="">
                           <option value="Yes">Yes</option>
                           <option value="No">No</option>
                       </select>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Money Back
                   </div>
                   <div class="col-md-9">
                       <select name="money-back" id="">
                           <option value="Yes">Yes</option>
                           <option value="No">No</option>
                       </select>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-3">
                       Other Info
                   </div>
                   <div class="col-md-9">
                       <textarea name="product-otherinfo" id="" cols="55" rows="3"></textarea>
                   </div>
               </div>
           </div>
           <div class="col-md-6 other-info">
               <div class="row">
                   <div class="col-md-4">
                       <h4 class="delivery-heading">Delivery Option</h4>
                       <!-- Squared TWO -->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredOne" name="within72hours" class="hidden" />
                                <label for="squaredOne"></label>
                            </div>
                            <span>Within 72 Hour</span>
                        </div>
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredTwo" name="withinDhaka" class="hidden" />
                                <label for="squaredTwo"></label>
                            </div>
                            <span>Within Dhaka</span>
                        </div>
-->
                   </div>
                   <div class="col-md-4">
                       <!-- Squared TWO -->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredThree" name="homeDelivery" class="hidden" />
                                <label for="squaredThree"></label>
                            </div>
                            <span>Home Delivery</span>
                        </div>
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredFour" name="within96hours" class="hidden" />
                                <label for="squaredFour"></label>
                            </div>
                            <span>Within 96 Hour</span>
                        </div>
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredFive" name="outofDhaka" class="hidden" />
                                <label for="squaredFive"></label>
                            </div>
                            <span>Out Of Dhaka</span>
                        </div>
-->
                   </div>
                   <div class="col-md-4">
                       <!-- Squared TWO -->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredSix" name="within24hours" class="hidden" />
                                <label for="squaredSix"></label>
                            </div>
                            <span>Within 24 Hour</span>
                        </div>
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredSeven" name="fromoffice" class="hidden" />
                                <label for="squaredSeven"></label>
                            </div>
                            <span>From Office</span>
                        </div>
-->
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-4">
                       <h4 class="delivery-heading">Select Payment Method</h4>
                   </div>
                   <div class="col-md-4">
                       <!-- Squared TWO -->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredEight" name="bank" class="hidden" />
                                <label for="squaredEight"></label>
                            </div>
                            <span>Bank</span>
                        </div>
-->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredNine" name="ucash" class="hidden" />
                                <label for="squaredNine"></label>
                            </div>
                            <span>Ucash</span>
                        </div>
-->
                   </div>
                   <div class="col-md-4">
                       <!-- Squared TWO -->
<!--
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredTen" name="bcash" class="hidden" />
                                <label for="squaredTen"></label>
                            </div>
                            <span>Bcash</span>
                        </div>
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Yes" id="squaredEleven" name="cashondelivery" class="hidden" />
                                <label for="squaredEleven"></label>
                            </div>
                            <span>Cash On Delivery</span>
                        </div>
-->
                   </div>
               </div>
               <button type="submit" name="add-product" class="submit-button pull-right">Submit</button>
           </div>
       </div>
   </div>
   </form>
   <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="product-list">
                    <?php foreach ($result as $item){ ?>

                  <div> <img src="<?php echo SITE_URL.'/assets/product_img/'.$item['image_1'] ?>"> </div>
                    <?php  } ?>
                  
                </div>
            </div>
        </div>
   </div>