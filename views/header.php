<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sylsell Login Page</title>
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo SITE_URL; ?>/assets/css/style.css">
</head>
<body>
<div class="tiny-bar">
    <?php if (isset($_SESSION['author-user'])){?>
    <a href="<?php echo SITE_URL?>/page/logout" class="pull-right">Logout</a>
    <?php }elseif (isset($_SESSION['user'])){?>
    <a href="<?php echo SITE_URL?>/page/logout" class="pull-right">Logout</a>
    <?php }?>
</div>