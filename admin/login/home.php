<?php
use App\Auth\Auth;
$auth = new Auth();
if (isset($_POST['admin_login'])){
//    print_r($_POST);
    $result = $auth->adminLogin($_POST);
}
?>
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 form-box">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Login to our site</h3>
                <p>Enter your username and password to log on:</p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-lock"></i>
            </div>
        </div>
        <div class="form-bottom">
            <form role="form" action="" method="post" class="login-form">
                <div class="form-group">
                    <label class="sr-only" for="form-username">UserEmail</label>
                    <input type="email" name="email" placeholder="Email..." class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="form-password">Password</label>
                    <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
                </div>
                <button name="admin_login" type="submit" class="btn">Sign in!</button>
            </form>
        </div>
    </div>
</div>