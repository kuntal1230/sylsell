<?php
use App\Product\Product;
$product = new Product();
$paging = new Product();
$per_page=2;
if(isset($_GET['name'])){
    $name=$_GET['name'];
    $condition="LEFT JOIN product_image ON products.id = product_image.product_id JOIN payment_option ON products.id = payment_option.product_id JOIN delivery_option ON products.id = delivery_option.product_id where product_name like '$name%'";
}
else{
    $condition="LEFT JOIN product_image ON products.id = product_image.product_id JOIN payment_option ON products.id = payment_option.product_id JOIN delivery_option ON products.id = delivery_option.product_id ";
}

$result = $product->viewProduct($per_page,$condition);
$current_url=$_SERVER['REQUEST_URI'];
if(isset($_GET['page'])){
    $page=$_GET['page'];
}
else{
    $page=1;
}


?>
   <div class="structure  container-fluid add-page">
        <div class="row">
            <div class="mini-logo  col-md-2">
               <img src="<?php echo SITE_URL ?>/assets/img/logo.png">
           </div><!-- end logo -->
           <div class="content col-md-10 sort">
                <span>Select Items</span>
               <?php
                   $url=$url=parse_url($_SERVER['REQUEST_URI']);
                   $cur_url_path=$url['path'].'/';
                   $cur_url_path=preg_replace('/\/+/','/',$cur_url_path)
               ?>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=a">A</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=b">B</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=c">C</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=d">D</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=e">E</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=f">F</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=g">G</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=h">H</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=i">I</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=j">J</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=k">K</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=l">L</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=m">M</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=n">N</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=o">O</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=p">P</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=q">Q</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=r">R</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=s">S</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=t">T</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=u">U</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=v">V</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=w">W</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=x">X</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=y">Y</a>
               <a href="<?php echo $cur_url_path ?>?page=<?php echo $page;?>&name=z">Z</a>
           </div>
        </div>
   </div>
   <form action="<?php echo SITE_URL ?>/page/order" method="post" onsubmit="return check()">
   <div class="container-fluid select-items">
       <div class="row">
            <div class="clearfix col-md-12">
                <span class="select-heading">Select Product</span>
            </div>
           <?php foreach ($result as $item){ ?>
           <div class="col-md-2 single-item">
               <label>
                   <span><?php echo $item['product_code']?></span>
                   <div class="photo">
                       <a href="<?php echo SITE_URL.'/page/view/?id='.$item['id'] ?>"><img src="<?php echo SITE_URL.'/assets/product_img/'.$item['image_1'] ?>"></a>
                       <input type="checkbox" name="<?php echo $item['product_code'] ?>" value="<?php echo $item['image_1'] ?>" id="item-select">
                   </div>
               </label>
           </div>
           <?php } ?>
           <div class="pagination col-md-12">
                <div class="pull-right ">
                    <?php $paging->paginate('products' , $per_page ,3,$condition);?>
<!--                   <a href="#">Next </a>/Or-->
                </div>
           </div> 
           <div class="col-md-12 text-right">
               <button class="place-order-button">Place Order</button>
           </div>
       </div>
   </div>
    </form>