<?php
use App\Product\Product;
$product = new Product();

$result = $product->viewProductById($_GET);

?>
<div class="structure  container-fluid add-page">
    <div class="row">
        <div class="mini-logo  col-md-2">
            <img src="<?php echo SITE_URL?>/assets/img/logo.png">
        </div><!-- end logo -->
        <div class="content col-md-10 sort">

        </div>
    </div>
</div>
<div class="container-fluid select-items">
    <div class="row">
        <div class="col-md-5">
            <div class="owl-carousel preview">
                <?php if($result['image_1']!="" || $result['image_1']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_1'] ?>">
                </div>
                <?php endif ?>
                <?php if($result['image_2']!="" || $result['image_2']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_2'] ?>">
                </div>
                <?php endif ?>
                <?php if($result['image_3']!="" || $result['image_3']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_3'] ?>">
                </div>
                <?php endif ?>
                <?php if($result['image_4']!="" || $result['image_4']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_4'] ?>">
                </div>
                <?php endif ?>
                <?php if($result['image_5']!="" || $result['image_5']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_5'] ?>">
                </div>
                <?php endif ?>
                <?php if($result['image_6']!="" || $result['image_6']!=null):?>
                <div class="photo">
                    <img src="<?php echo SITE_URL.'/assets/product_img/'.$result['image_6'] ?>">
                </div>
                <?php endif ?>

            </div>
        </div>
        <div class="col-md-7 basic-info">
            <div class="row">
                <div class="">
                    <span class="description-heading">Product Description</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Name Of Product
                </div>
                <div class="col-md-9">
                    <span> <?php echo $result['product_name']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Made In
                </div>
                <div class="col-md-9">
                    <span><?php echo $result['product_madein']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Warrenty
                </div>
                <div class="col-md-9">
                    <span><?php echo $result['product_warrenty']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Guarrenty
                </div>
                <div class="col-md-9">
                    <span><?php echo $result['product_garrenty']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Replacement
                </div>
                <div class="col-md-9">
                    <span><?php echo $result['product_replacement']?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Money Back
                </div>
                <div class="col-md-9">
                    <span><?php echo $result['product_moneyback']?></span>
                </div>
            </div>
            <div class="row view-delivery">
                <div class="col-md-4">
                    <h4 class="delivery-heading">Delivery Option</h4>
                    <!-- Squared TWO -->

                    <div class="single-checkbox">
                        <?php if ($result['within72hours']=='Yes'){ ?>
                        <span>Within 72 Hour</span>
                        <?php } ?>
                    </div>

                    <div class="single-checkbox">
                        <?php if ($result['withindhaka']=='Yes'){ ?>
                        <span>Within Dhaka</span>
                        <?php }  ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <!-- Squared TWO -->
                    <div class="single-checkbox">
                        <?php if ($result['homedelivery']=='Yes'){ ?>
                        <span>Home Delivery</span>
                        <?php }  ?>
                    </div>
                    <div class="single-checkbox">
                        <?php if ($result['within96hours']=='Yes'){ ?>
                        <span>Within 96 Hour</span>
                        <?php }  ?>
                    </div>
                    <div class="single-checkbox">
                        <?php if ($result['outofdhaka']=='Yes'){ ?>
                        <span>Out Of Dhaka</span>
                        <?php }  ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Squared TWO -->
                    <div class="single-checkbox">
                        <?php if ($result['within24hours']=='Yes'){ ?>
                        <span>Within 24 Hour</span>
                        <?php }  ?>
                    </div>
                    <div class="single-checkbox">
                        <?php if ($result['fromoffice']=='Yes'){ ?>
                        <span>From Office</span>
                        <?php }  ?>
                    </div>
                </div>
            </div>
            <button class="place-order-button view-order-button">Place Order</button>
        </div>
    </div>
</div>
<div class="container-fluid view-other">
    <div class="row">
        <div class="col-md-2 other-heading">
                <span>
                    Other Option
                </span>
        </div>
        <div class="col-md-12 other-body">
            <div >
                selects all elements except the specified element. This is mostly used together with another selector to select everything ... selects all elements except the specified element. This is mostly used together with another selector to select everything ...
            </div>

        </div>
    </div>
</div>
