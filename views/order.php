  <?php
  use App\Product\Product;
  $product = new Product();
  if (isset($_POST)){
  }
  if(isset($_POST['order'])){
      echo $product->order();

  }
  ?>
   <form action="" method="post" onsubmit="return product_final_select()" class="order-form">
   <div class="structure  container-fluid add-page order-page">
        <div class="row">
            <div class="mini-logo  col-md-2">
               <img src="<?php echo SITE_URL ?>/assets/img/logo.png">
           </div><!-- end logo -->
           <div class="content col-md-10 select-items">
                <div class="row">
                    <div class="clearfix col-md-12">
                        <span class="select-heading">Select Product</span>
                    </div>
                <?php foreach ($_POST as $key=>$value){?>
                   <div class="col-md-2 single-item final-select">
                       <label>
                           <?php
                           $product_info=$product->product_info_by_code($key);
                           ?>
                           <span><?php echo $key ?></span>
                           <div class="photo">
                               <img src="<?php echo SITE_URL.'/assets/product_img/'.$value?>">
                               <input type="checkbox" value="<?php echo $product_info['product_code'];?>" class="product_final_select" checked check="true">
                           </div>

                           <input type="hidden" name="product_code[]" value="<?php echo $product_info['product_code'];?>">
                           <input type="hidden" id="product_price" value="<?php echo $product_info['product_price'];?>">
                       </label>
                       <div class="quantity-maintain">
                           <span class="quantity-heading">Qnt:</span>
                           <span class="qt_minus">-</span>
                           <input type="text" class="quantity-field" value="1" name="quantity[]">
                           <span class="qt_plus">+</span>
                       </div>
                   </div>
                    <?php } ?>
               </div>
           </div>
        </div>
   </div>
   
   <div class="container-fluid select-items">
      <p class="order-form-heading container-fluid">
        Order Form <span>Time:<?php echo date("h:ia")?></span>
      </p>
      <div class="container-fluid order-input-place">
          <div class="order-number">
               To
              <p>
                  Order No.   
                  <span>
                      <?php
                      $user_id=$_SESSION['author-user']['id'];
                      $order_number="SS".$user_id.date("s")."-".date("y");
                      ?>
                     <?php echo $order_number;?>
                      <input type="hidden" name="order_id" value="<?php echo $order_number;?>">
                      <input type="hidden" name="uploader" value="<?php echo $user_id;?>">
                  </span>
              </p>
          </div>
          <div class="clearfix"></div>
          <div class="product-amount-info">
              Sylsell Online Shop
              <p>
                  Total Quantity
                  <span class=""><label class="total_qnt"></label>ps</span>
                  <input type="hidden" value="0" name="total_quantity" class="total_qnt_value">
                  Total Amount
                  <span class="">TK.<label class="total_amount"></label></span>
                  <input type="hidden" value="0" name="total_price" class="total_price_value">
              </p>

          </div>
          <div class="clearfix"></div>
          <div class="row single-input">
              <div class="field-heading col-md-3">
                  Name
              </div>
              <div class="field-body col-md-9">
                  <input type="text" placeholder="Type Your Name Here." name="name">
              </div>
          </div>
          <div class="row single-input">
              <div class="field-heading col-md-3">
                  Contact Number
              </div>
              <div class="field-body col-md-9">
                  <input type="text" placeholder="Type Your Mobile Number Here." name="contact_number">
              </div>
          </div>
          <div class="row single-input">
              <div class="field-heading col-md-3">
                  Order Description
              </div>
              <div class="field-body col-md-9">
                  <input type="text" placeholder="(Code#SS1052=3Pcs), (Code#SS1024=1Pcs), (Code#SS1052=3Pcs), (Code#SS1024=1Pcs)" name="product_codes">
              </div>
          </div>
          <div class="row single-input">
              <div class="field-heading col-md-3">
                  Delivery Address
              </div>
              <div class="field-body col-md-9">
                  <input type="text" placeholder="Type Your Full Address Here" name="delevery_address">
              </div>
          </div>
<!--          <div class="row single-input">-->
<!--              <div class="field-heading col-md-3">-->
<!--                  Delivery Option-->
<!--              </div>-->
<!--              <div class="field-body col-md-9">-->
<!--                  <div class="row">-->
<!--                      <div class="col-md-3">-->
<!--                        <h4 class="delivery-heading">Select District</h4>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredOne" name="check" class="hidden" />-->
<!--                                <label for="squaredOne"></label>-->
<!--                            </div>-->
<!--                            <span>Within 72 Hour</span>-->
<!--                        </div>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredTwo" name="check" class="hidden" />-->
<!--                                <label for="squaredTwo"></label>-->
<!--                            </div>-->
<!--                            <span>Within Dhaka</span>-->
<!--                        </div>-->
<!--                      </div>-->
<!--                      <div class="col-md-3">-->
<!--                        <select class="delivery-heading select-district">-->
<!--                            <option>Khulna</option>-->
<!--                            <option>Rangpur</option>-->
<!--                            <option>Kusthia</option>-->
<!--                            <option>Dhaka</option>-->
<!--                        </select>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredOne" name="check" class="hidden" />-->
<!--                                <label for="squaredOne"></label>-->
<!--                            </div>-->
<!--                            <span>Within 72 Hour</span>-->
<!--                        </div>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredTwo" name="check" class="hidden" />-->
<!--                                <label for="squaredTwo"></label>-->
<!--                            </div>-->
<!--                            <span>Within Dhaka</span>-->
<!--                        </div>-->
<!--                      </div>-->
<!--                      <div class="col-md-3">-->
<!--                        <h4 class="delivery-heading">Country</h4>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredOne" name="check" class="hidden" />-->
<!--                                <label for="squaredOne"></label>-->
<!--                            </div>-->
<!--                            <span>Within 72 Hour</span>-->
<!--                        </div>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredTwo" name="check" class="hidden" />-->
<!--                                <label for="squaredTwo"></label>-->
<!--                            </div>-->
<!--                            <span>Within Dhaka</span>-->
<!--                        </div>-->
<!--                      </div>-->
<!--                      <div class="col-md-3">-->
<!--                        <select class="delivery-heading select-country">-->
<!--                            <option>Bangladesh</option>-->
<!--                            <option>America</option>-->
<!--                            <option>India</option>-->
<!--                            <option>Pakistan</option>-->
<!--                        </select>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredOne" name="check" class="hidden" />-->
<!--                                <label for="squaredOne"></label>-->
<!--                            </div>-->
<!--                            <span>Within 72 Hour</span>-->
<!--                        </div>-->
<!--                        <div class="single-checkbox">-->
<!--                            <div class="squaredTwo">-->
<!--                                <input type="checkbox" value="None" id="squaredTwo" name="check" class="hidden" />-->
<!--                                <label for="squaredTwo"></label>-->
<!--                            </div>-->
<!--                            <span>Within Dhaka</span>-->
<!--                        </div>-->
<!--                      </div>-->
<!--                      -->
<!--                  </div>-->
<!--              </div>-->
<!--          </div>-->
          <div class="row single-input">
              <div class="field-heading col-md-3">
                  Select Payment Method
              </div>
              <div class="field-body col-md-9">
                  <div class="row">
                      <div class="col-md-3">
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Bank" id="squaredTwo" name="payment_method" class="hidden method" />
                                <label for="squaredTwo"></label>
                            </div>
                            <span>Bank</span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Bkash" id="squaredthree" name="payment_method" class="hidden method" />
                                <label for="squaredthree"></label>
                            </div>
                            <span>Bkash</span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="U-cash" id="squaredFour" name="payment_method" class="hidden method" />
                                <label for="squaredFour"></label>
                            </div>
                            <span>U-cash</span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="single-checkbox">
                            <div class="squaredTwo">
                                <input type="checkbox" value="Cash On Delivery" id="squaredFive" name="payment_method" class="hidden method" />
                                <label for="squaredFive"></label>
                            </div>
                            <span>Cash On Delivery</span>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="aggrement-button">
                <div class="single-checkbox">
                    <div class="squaredTwo">
                        <input type="checkbox" value="None" id="aggree" name="check" class="hidden" />
                        <label for="aggree"></label>
                    </div>
                    <span>I have made this order to purchase the above mentioned item(s), Please provide me to as I have filled out the order form</span>
                </div>
          </div>
          <div class="order-submit-button">
              <button name="order">Submit Order</button>
          </div>
      </div> 
   </div>
   </form>