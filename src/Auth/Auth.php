<?php
/**
 * Created by PhpStorm.
 * User: EXPERT SOFT IT
 * Date: 4/25/2017
 * Time: 12:15 PM
 */

namespace App\Auth;


use App\Dbconect\Dbconect;
use PDO;

class Auth extends Dbconect
{
    public function adminLogin($data = ''){
        if (isset($data['email'])){
            $email = $data['email'];
        }

        if (isset($data['password'])){
            $password =md5($data['password']);
        }

        $pdo = $this->db;
        $query = 'SELECT * FROM admin WHERE user_email = :mail AND user_password = :pass';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':mail' => $email,
            ':pass' => $password

        );
        $stmt->execute($data);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            $_SESSION['admin'] = $result;

            header('location:' . SITE_URL . '/admin/home');
        }
        else{
            return "User Name Or Password Not Correct!";
        }
    }

    public function register($data = ''){
        if (isset($data['name'])){
            $name = $data['name'];
        }
        if (isset($data['number'])){
            $number = $data['number'];
        }
        if (isset($data['email'])){
            $email = $data['email'];
        }
        if (isset($data['password'])){
            $password = $data['password'];
        }
        if (isset($data['re-password'])){
            $repassword =$data['re-password'];
        }

        if ($password != $repassword){
            return $error = "Password don't match";
        }elseif (empty($name) && empty($number)&& empty($email)){
            return $error = "All field required";
        }else{
            $pdo = $this->pdo;
            $query='select * from users where user_mobile=:mobile OR user_email =:email';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(':mobile', $number);
            $stmt->bindValue(':email', $email);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if($row > 0) {
                if ($row['user_mobile'] == $number) {
                    return $error = "Number Already Exiest!!!!!";
                } elseif ($row['user_email'] == $email) {
                    return $error = "Email Already Exiest!!!!!";
                }

            }else{
                $pdo = $this->pdo;
                $query = 'INSERT INTO `users` (`user_name`, `user_mobile`, `user_email`, `user_password`, `created_at`) VALUES(:username, :number, :email, :password ,:time)';
                $time = date('Y-m-d H:i:s');
                $stmt = $pdo->prepare($query);
                $data = array(
                    ':username' => $name,
                    ':number' => $number,
                    ':email' => $email,
                    ':password' => $password,
                    ':time' => $time
                );
                $status = $stmt->execute($data);
                $lastid = $pdo->lastInsertId();
                if ($status) {
                    $pdo = $this->pdo;
                    $query = 'SELECT * FROM users WHERE id = :id';
                    $stmt = $pdo->prepare($query);
                    $data = array(
                        ':id' => $lastid,

                    );
                    $stmt->execute($data);

                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    if ($stmt->rowCount() > 0) {
                        $_SESSION['user'] = $result;

                        header('location:' . SITE_URL . '/page/product');
                    }
                }//end or insert
            }

         }
    }

    public function logim($data = ''){
        if (isset($data['number'])){
            $number = $data['number'];
        }

        if (isset($data['password'])){
            $password = $data['password'];
        }

        $pdo = $this->db;
        $query = 'SELECT * FROM users WHERE user_mobile = :num AND user_password = :pass';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':num' => $number,
            ':pass' => $password

        );
        $stmt->execute($data);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            $_SESSION['user'] = $result;

            header('location:' . SITE_URL . '/page/product');
        }
        else{
            return "User Name Or Password Not Correct!";
        }

    }

    public function authorisetion($data = ''){
        if (isset($data['code'])){
            $authorcode = $data['code'];
        }
        $pdo = $this->db;
        $query = 'SELECT * FROM authorisetion WHERE authorised_code = :code';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':code' => $authorcode

        );
        $stmt->execute($data);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            $_SESSION['author'] = $result;

            header('location:' . SITE_URL . '/page/member');
        }
        else{
            return "Inserted Number Not Correct!";
        }

    }
    public function authorisedUserRegister($data = ''){
        if (isset($data['associatName'])){
            $name = $data['associatName'];
        }
        if (isset($data['number'])){
            $number = $data['number'];
        }
        if (isset($data['email'])){
            $email = $data['email'];
        }
        if (isset($data['area'])){
            $area = $data['area'];
        }
        if (isset($data['businessName'])){
            $business = $data['businessName'];
        }
        if (isset($data['password'])){
            $password = $data['password'];
        }
        if (isset($data['re-password'])){
            $repassword =$data['re-password'];
        }
       if ($password != $repassword){
            return "Password don't match";
        }elseif (empty($name) && empty($number)&& empty($email)) {
           return $error = "All field required";
       }else{
           $pdo = $this->db;
           $query='select * from authorised_user where authorise_user_number=:mobile OR authorise_user_email =:email';
           $stmt = $pdo->prepare($query);
           $stmt->bindValue(':mobile', $number);
           $stmt->bindValue(':email', $email);
           $stmt->execute();
           $row = $stmt->fetch(PDO::FETCH_ASSOC);
           if($row > 0) {
               if ($row['authorise_user_number'] == $number) {
                   return $error = "Number Already Exiest!!!!!";
               } elseif ($row['authorise_user_email'] == $email) {
                   return $error = "Email Already Exiest!!!!!";
               }

           }
           else{

                $pdo = $this->db;
                $query = 'INSERT INTO `authorised_user` (`associate_name`, `authorise_user_number`, `authorise_user_email`, `name_of_area`, `name_of_business`, `password`, `created_at`) VALUES(:associatname, :number, :email, :area, :business, :password, :time)';
                $time = date('Y-m-d H:i:s');
                $stmt = $pdo->prepare($query);
                $data = array(
                    ':associatname' => $name,
                    ':number' => $number,
                    ':email' => $email,
                    ':area' => $area,
                    ':business' => $business,
                    ':password' => $password,
                    ':time' => $time
                );
                $status = $stmt->execute($data);
                $lastid = $pdo->lastInsertId();
                if ($status) {
                    $pdo = $this->db;
                    $query = 'SELECT * FROM authorised_user WHERE id = :id';
                    $stmt = $pdo->prepare($query);
                    $data = array(
                        ':id' => $lastid,

                    );
                    $stmt->execute($data);

                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    if ($stmt->rowCount() > 0) {
                        $_SESSION['author-user'] = $result;

                        header('location:' . SITE_URL . '/page/add-product');
                    }
                }//end or insert
            }

        }
    }

    public function authorisedUserLogin($data = ''){
        if (isset($data['number'])){
            $number = $data['number'];
        }

        if (isset($data['password'])){
            $password = $data['password'];
        }

        $pdo = $this->db;
        $query = 'SELECT * FROM authorised_user WHERE authorise_user_number = :num AND password = :pass AND activity = 1';
        $stmt = $pdo->prepare($query);
        $data = array(
            ':num' => $number,
            ':pass' => $password

        );
        $stmt->execute($data);

        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            $_SESSION['author-user'] = $result;

            header('location:' . SITE_URL . '/page/add-product');
        }

    }

    public function getAuthorisedUser(){
        $pdo = $this->db;
        $query = 'SELECT * FROM authorised_user';
        $stmt = $pdo->query($query);

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function adminAction(){

    }

    public function logOut(){
        session_destroy();
        unset($_SESSION);
        header('location: ' . SITE_URL . '/page/home');
        return true;
    }
    public function AdminlogOut(){
        session_destroy();
        unset($_SESSION);
        header('location: ' . SITE_URL . '/login/home');
        return true;
    }
}