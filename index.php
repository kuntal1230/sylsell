<?php
ob_start();
session_start();
error_reporting();
include "define.php";
include_once (__DIR__. "/vendor/autoload.php");

$req = explode('/',$_SERVER['REQUEST_URI']);
if ($req[1]=='login'){
    $path = "admin/login/";
}elseif($req[1]=='admin') {
    $path = "admin/views/";
}else{
    $path = "views/";
}
    include $path . "header.php";
if(isset($req[2])){
	$page = $req[2];
	if(!empty($page)){
        if(file_exists($path.$page.".php")){
            include $path.$page.".php";
        }else{
            include $path . "404.php";
        }
    }else{
            include $path . "home.php";
        }
}else{
	include $path . "home.php";
}
include $path . "footer.php";
ob_flush();
?> 	

	