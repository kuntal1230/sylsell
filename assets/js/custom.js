$(document).ready(function(){
    $(".home-slider").owlCarousel({
        items:10,
        autoplay:true,
        loop:true
    });
    $(".preview").owlCarousel({
        items:1,
//        autoplay:true,
        loop:true,
        dots:true
    });
    $(".owl-dots .owl-dot").remove();
    var active_item=$(".preview .owl-item.active").attr("src");
    $(".preview .owl-item:not(.cloned)").each(function(){
        var img=$("img",this).attr("src");
        if(active_item==img){
            var thumb_img="<div class='owl-dot active'><img src='"+img+"'></div>";
        }
        else{
            var thumb_img="<div class='owl-dot'><img src='"+img+"'></div>";
        }
        $(".owl-dots").append(thumb_img);
    });


    //item preview before upload
    function preview(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item1 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item1 input").change(function(){
        preview(this);
    });

//item preview before upload
    function preview2(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item2 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item2 input").change(function(){
        preview2(this);
    });

//item preview before upload
    function preview3(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item3 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item3 input").change(function(){
        preview3(this);
    });

//item preview before upload
    function preview4(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item4 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item4 input").change(function(){
        preview4(this);
    });

//item preview before upload
    function preview5(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item5 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item5 input").change(function(){
        preview5(this);
    });
//item preview before upload
    function preview6(input){
        if(input.files && input.files[0]){
            var reader=new FileReader();
            reader.onload=function(e){
                // $("#upload_item ").show();
                $(".upload_item.item6 img").attr('src',e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".upload_item.item6 input").change(function(){
        preview6(this);
    });

    //quantity maintain

    // $(".qt_minus").click(function () {
    //     var qnt_number=$(this).parent().children(".quantity-field").val();
    //     if(Number(qnt_number)>1){
    //         var dec_number=Number(qnt_number)-1;
    //         var qnt_number=$(this).parent().children(".quantity-field").val(dec_number);
    //         total_qnt();
    //         total_price();;
    //     }
    //
    // });
    //
    // $(".qt_plus").click(function () {
    //     var qnt_number=$(this).parent().children(".quantity-field").val();
    //     var inc_number=Number(qnt_number)+1;
    //     var qnt_number=$(this).parent().children(".quantity-field").val(inc_number);
    //     total_qnt();
    //     total_price();
    // });
    $(".qt_minus").click(function () {
        var qnt_number=$(this).parent().children(".quantity-field").val();
        if(Number(qnt_number)>1){
            var dec_number=Number(qnt_number)-1;
            var qnt_number=$(this).parent().children(".quantity-field").val(dec_number);
            var item_stat=$(this).parent().parent().children("label").children(".photo").children(".product_final_select").prop("checked");
            if(item_stat==true){
                total_qnt();
                total_price();
            }
        }

    });

    $(".qt_plus").click(function () {
        var qnt_number=$(this).parent().children(".quantity-field").val();
        var inc_number=Number(qnt_number)+1;
        var qnt_number=$(this).parent().children(".quantity-field").val(inc_number);
        var item_stat=$(this).parent().parent().children("label").children(".photo").children(".product_final_select").prop("checked");
        if(item_stat==true){
            total_qnt();
            total_price();
        }

    });



    function total_price() {
        var total_price=0;
        $(".final-select").each(function () {
            var product_price= Number($("#product_price",this).val());
            var quantity= Number($(".quantity-field",this).val());
             total_price +=product_price*quantity;

        });
        $(".total_amount").text(total_price);
        $(".total_price_value").val(total_price);
    }
    total_price();

    function total_qnt() {
        var sum=0;
        $(".quantity-field").each(function () {
            sum +=Number($(this).val());
        });
        $(".total_qnt").text(sum);
        $(".total_qnt_value").val(sum);
    }
    total_qnt();

    function tot_qnt() {
        total_qnt();
    };
        $(".product_final_select").click(function () {
            var stat=$(this).prop("checked");
            if(stat==false){
                var total_amount=Number($(".total_price_value").val());
                var total_qnt=Number($(".total_qnt_value").val());
                var this_price=Number($(this).parent().parent().children("#product_price").val());
                var this_qnt=Number($(this).parent().parent().parent().children(".quantity-maintain").children(".quantity-field").val());
                var minus_price=total_amount-(this_price*this_qnt);
                var minus_qnt=total_qnt-this_qnt;
                $(".total_qnt").text(minus_qnt);
                $(".total_qnt_value").val(minus_qnt);
                $(".total_amount").text(minus_price);
                $(".total_price_value").val(minus_price);
            }
            else if(stat==true){
                total_price();
                tot_qnt();
            }
        });








    $(".order-form").submit(function () {
        var ele=$(".method:checked");
        var ele_lenth=ele.length;
        if(ele_lenth<1){
            alert("Select A Payment Method");
            return false;
        }
    });




});

function check() {
    var ele=$("input[type='checkbox']:checked");
    var ele_lenth=ele.length;
    if(ele_lenth<1){
        alert("Please Select At Least One Item.");
        return false;
    }
}

function product_final_select() {
    var ele=$(".product_final_select:checked");
    var ele_lenth=ele.length;
    if(ele_lenth<1){
        alert("Please At Least One Order Item .");
        return false;
    }
}

