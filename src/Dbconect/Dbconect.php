<?php
/**
 * Created by PhpStorm.
 * User: EXPERT SOFT IT
 * Date: 4/20/2017
 * Time: 11:53 AM
 */

namespace App\Dbconect;
use PDO;

class Dbconect
{
    protected $db;
    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=sylsell_shop', 'root', '') ;
    }

    /**
     * @return PDO
     */
    public function Db()
    {
        return $this->db;
    }

}