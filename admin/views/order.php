<?php
use App\Product\Product;
$orderlist = new Product();
$paging = new Product();
$per_page=2;

$condition="LEFT JOIN order_items ON orders.order_id = order_items.order_id  JOIN products ON order_items.product_code = products.product_code ";

$result = $orderlist->orderList($per_page,$condition);
$current_url=$_SERVER['REQUEST_URI'];
if(isset($_GET['page'])){
    $page=$_GET['page'];
}
else{
    $page=1;
}

?>
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Order list
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Customar Name</th>
                                    <th>Order Id</th>
                                    <th>Quentity</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($result as $value) { ?>
                                <tr>
                                    <td><?php echo  $value['name']?></td>
                                    <td><?php echo  $value['order_id']?></td>
                                    <td><?php echo  $value['total_quantity']?></td>
                                    <td>
                                        <a href="<?php echo SITE_URL.'/admin/view/?id='.$value['order_id']?>">View</a>
                                        <?php if ($value['activity']==0) {?>
                                        ||
                                        <a href="<?php echo SITE_URL.'/admin/confirm/?id='.$value['order_id']?>">Confirm</a>
                                        <?php } else{ echo " || Confirmed";}?>
                                    </td>
                                </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                            <div class="pagination col-md-12">
                                <div class="pull-right ">
                                    <?php $paging->paginate('orders' , $per_page ,3,$condition);?>
                                    <!--                   <a href="#">Next </a>/Or-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. ROW  -->